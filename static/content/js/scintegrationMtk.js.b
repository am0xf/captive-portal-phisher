﻿
/*************************************
Integration with Mikrotik
***********************/

var eIP;
var eMAC;
var authStatusCheck;
var authCheckCnt = 0;


function getTimeout() {
    var rez = window.location.search.match(/c_timeout=(\d+)/)
    if (rez && rez.length == 2) {
        return parseInt(rez[1], 10)
    }
    return -1;  // No timeout found
}


$(document).ready(function () {

    if (document.sendin.mk_error.value != "") {
        swal({
            title: document.sendin.mk_error.value,
            type: "error",
        });
    } else {
        if ($("#linkSurfURL").attr("href") != undefined) {
            if ($("#linkSurfURL").attr("href").indexOf("cpOLPay") >= 0) {
                window.location = $("#linkSurfURL").attr("href");
            }
            else if ($("#linkSurfURL").attr("href") != "about:blank" && $("#linkSurfURL").attr("href") != "") {
                OpenInNewTab(linkSurfURL);
            }
        }
        //No errr message

        // IF Success login
        // Gets the timeout from the URL
        /*  c_timeout = getTimeout();
          if (c_timeout != -1) {
              // Login Success
              if (status == "Trial") {
                  window.location = document.getElementById('hndPortalURL').value + "/Customer/RequestBSNL.aspx?src=cpOLPay&" + onlinePurchaseReciept;
              } else {
                  swal({
                      title: "You have been logged in successfully, Enjoy wifi.",
                      type: "success",
                  });
                  //settimer();
                  LoginOpen();
                  openStatus();
                  eventFire(document.getElementById('click'), 'click');
                  document.getElementById('dvStatus').style.display = 'block';
              }
          }*/
    }
});

function Authenticate() {
    eIP = $("#hdnIPAddress").val();
    eMAC = $("#hdnMACAddress").val();
    status = $("#custStatus").val();
    var chapid = document.sendin.mk_chapid.value;
    var chapchallenge = document.sendin.mk_chapchallenge.value;
    onlinePurchaseReciept = $("#onlinePurchaseReciept").val();


    document.sendin.mk_username.value =$("#cpusername").val() + $("#hdnhotspotgroup").val();
    if ($("#mk_chapid").val() != null && $("#mk_chapid").val() != "") {
        document.sendin.mk_password.value = hexMD5($("#mk_chapid").val() + $("#cppassword").val() + $("#mk_chapchallenge").val());
    }
    else {
        document.sendin.mk_password.value = $("#cppassword").val();
    }

    if (onlinePurchaseReciept != '') {
		
		// window.location.replace('https://customer.i-on.in/');
        document.sendin.dst.value = 'https://customer.i-on.in/'; //document.getElementById('hndPortalURL').value + "/Customer/RequestBSNL.aspx?src=cpOLPay&" + onlinePurchaseReciept;
    }
    else if ($("#linkSurfURL").attr("href") != "about:blank" && $("#linkSurfURL").attr("href") != "") {
        document.sendin.dst.value = $("#linkSurfURL").attr("href");
    }
    document.sendin.submit();
    return false;
}

function sclogout() {
    /*
     // url: 'ScIntegration.ashx?Auth=1&eIP=' + eIP + '&eMAC=' + eMAC + '&username=' + $("#loginCountry").val() + $("#username").val() + '&password=' + $("#password").val(),
     document.sendin.mk_username.value = $("#loginCountry").val() + $("#username").val();
     if (chapid != null && chapid != "") {
         document.sendin.mk_password.value = hexMD5(chapid + $("#password").val() + chapchallenge);
     }
     else {
         document.sendin.mk_password.value = $("#password").val();
     }*/
    document.logout.submit();
    return false;

}
