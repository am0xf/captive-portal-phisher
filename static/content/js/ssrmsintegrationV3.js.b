﻿
$(document).ready(function () {

    try {

        ChangeUrl('login');

        $(window).bind("popstate", function (event) {

            updateState(history.state);
        });
		 if (getCookie('Rememberme') != null && getCookie('Rememberme') != "" && getCookie('Rememberme') == "1") {
            $("#chkrem").attr("checked", "checked");
            $("#cpusername").val(getCookie('Uname'));
            $("#cppassword").val(getCookie('Pword'));
        }
        $('#chkrem').click(function () {
            if ($("#cpusername").val() == "") {
                swal({
                    title: "Enter Username",
                    type: "warning",
                });
                return false;
            }
            if ($("#cppassword").val() == "") {
                swal({
                    title: "Enter Password",
                    type: "warning",
                });
                return false;
            }
            if ($('#chkrem').is(':checked')) {
                // save username and password
                setCookie('Rememberme', '1', 30);
                setCookie('Uname', $("#cpusername").val(), 30);
                setCookie('Pword', $("#cppassword").val(), 30);
                // setCookie('Pword', Encrypt($("#cppassword").val()), 30);
            } else {
                setCookie('Rememberme', '0', 30);
                setCookie('Uname', '', 30);
                setCookie('Pword', '', 30);
            }
        });

    } catch (e) {

    }

    if ($("#hndRespMsg").val() != "") {
        sweetAlert($("#hndRespMsg").val());
    }

    if ($("#hndReqId").val() != "") {
        if ($("#hdnloginval").val() == "") {
            sclogout();
        }
        GetUserByrequestId();
    }


    if (getCookie('LogoutClick') != "" && getCookie('LogoutClick') == "1") {
        swal({
            title: "You have been Logout successfully !",
            type: "success",
        });
        setCookie('LogoutClick', '0', 1);
        return false;
    }


    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
        HitsCount("Welcome");
    }

    GetApDetail();

    $("#btnLogin").click(function () {
        return CheckCustomer();
    });

    $("#btnLogout").click(function () {
        return sclogout();
    });


    $("#btnOTPVerify").click(function () {
        return OTPVerfication();
    });


    $("#loginCountry").click(function () {
        return GetCountryList(this);
    });

    $("#resendPin").click(function () {
        var obj = { otpevent: 'freeregistration' }
        getOtp(obj);
    });

    $("#forgotPin").click(function () {
        Forgotpasswrod();
    });

    $("#btnForgotOTPVerify").click(function () {
        ForgotPasswordVerifyUpdate();
    });

    $("#btnReLogin").click(function () {
        window.location.href = window.location.href.replace('login=1', '').replace('#', '');
    });

    $("#btnViewPlan").click(function () {
        GetPlanList();
        ChangeUrl('viewplan');
        return false;
    });

    $(".history_back").click(function () {
        history.back();
        return false;
    });

    $("#aFreeData").click(function () {
        AskToUser();
        return false;
    });

    $("#btnPaidPlan").click(function () {


        if ($("#cpusername").val() == "") {
            swal({
                title: "Please enter mobile number !",
                type: "warning",
            });
            return false;
        }
        OpenRenewbox();
        $("#dvvoucherbtn").hide();
        $("#dvvoucherpin").hide();
        $("#dvPaidPlan").show();
        $("#dvpgline").show();
        return false;
    });

    $("#btnGoVoucherPlan").click(function () {


        if ($("#cpusername").val() == "") {
            swal({
                title: "Please enter mobile number !",
                type: "warning",
            });

            return false;
        }
        OpenRenewbox();
        $("#dvvoucherbtn").show();
        $("#dvvoucherpin").show();
        $("#dvPaidPlan").hide();
        $("#dvpgline").hide();
        return false;
    });

    $("#btnVoucher").click(function () {


        if ($("#cpusername").val() == "") {
            swal({
                title: "Please enter mobile number !",
                type: "warning",
            });
            return false;
        }

        if ($('#cppassword').val() != "") {
            RenewVoucher();
            return false;
        }
        else {

            $('#otpevent').val('voucherregistration');
            CheckCustomer();
            return false;
        }
    });

    $("#btnselfcare").click(function () {
        var url = $('#UrlRedirectToSelfCare').data('url');
        setCookie('SelfCareusername', $("#loginCountry").val() + $("#cpusername").val() + $("#hdnhotspotgroup").val(), 1);
        //var url = document.getElementById('hndselfcareurl').value + "?Uname=" + $("#loginCountry").val() + $("#cpusername").val() + $("#hdnhotspotgroup").val() + "&Pass=" + getCookie('password') + "&Hid=" + $("#hotspotID").val();
        try {
            window.open(url, '_blank').focus();
        } catch (e) {
            window.location.href = url;
            console.log(e.message);
        }
        //window.location.href = url;
        return false;
    });

    $("#dvslider").find("img").addClass("img-responsive").css('height', 'auto');
    $("#dvslider").find("div").css('position', 'inherit');

    $("#dvsliderPostLogin").find("img").addClass("img-responsive").css('height', 'auto');
    $("#dvsliderPostLogin").find("div").css('position', 'inherit');
});

function updateState(state) {
    if (state != null) {
        var params = getRequestString();

        if (params['page'] != null) {
            ChangeUrl(params['page']);
        }
        else {
            ChangeUrl('login');
        }
    }
    return false;
}

function getRequestString() {
    var match,
       pl = /\+/g,  // Regex for replacing addition symbol with a space
       search = /([^&=]+)=?([^&]*)/g,
       decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
       query = window.location.search.substring(1);

    urlParams = {};
    while (match = search.exec(query))
        urlParams[decode(match[1])] = decode(match[2]);

    return urlParams;
}


function ChangeUrl(page) {

    var params = getRequestString();

    if (params['login'] != null && params['login'] == "1" && page != 'logout' && params['res'] != 'logoff') {
        page = 'status';
    }
    try {
        if (typeof (history.pushState) != "undefined") {

            params['page'] = page;

            var obj = { Page: page, Url: window.location.href.split('?')[0] + '?' + decodeURIComponent($.param(params)) };

            if ($('.current_page').attr('id') == page) {
                history.replaceState(obj, obj.Page, obj.Url);
            }
            else {
                history.pushState(obj, obj.Page, obj.Url);
            }
        }

        $('#dvslider').css('display', 'block');
        $('#dvsliderPostLogin').css('display', 'none');
        if (page == 'login') {
            try {
                $('#bottomlogo').hide();
                $('#cpusername').focus();
                $('#stspagelogo').hide();
				$('#stsfedbck').hide();
            } catch (e) {
                console.log(e.message);
            }
        }
        else if (page == 'otp') {
            try {
                $('#cppassword').focus();
            } catch (e) {
                console.log(e.message);
            }
        }
        else if (page == 'viewplan' || page == 'paidplan' || page == 'hybridplan') {
            $('#offerStatement').css('display', 'none');
            $('#dvslider').css('display', 'none');
        }
        else if (page == 'status') {
			$('#dvslider').css('display', 'none');
            $('#status').css('display', 'block');
            $('#bottomlogo').show();
            $('#logcheck').hide();
			$('#stsfedbck').show();
            $("#cpusername").val(getCookie('username'));
            $("#LoginSuccessful").val("1");
            if (getCookie('hotspotgroup') != null && getCookie('hotspotgroup') != '' && getCookie('hotspotgroup') != "") {
                $("#hdnhotspotgroup").val(getCookie('hotspotgroup'));
            }
            else {
                $("#hdnhotspotgroup").val("");
            }
            ViewCurrentPlanDetails();
            GetConcurrentSession();
			
            //if ($('#hdnpostaddintr').val() != "") {

            //    document.getElementById('divpostadd').style.display = 'block';
            //    document.getElementById('divpostadd').innerHTML = $('#hdnpostaddintr').val();
            //    var ads = $('#divpostadd').html();
            //    $('#divpostadd').html(ads);

            //}
        }
        else if (page == 'logout') {
            $('#dvslider').css('display', 'none');
            $('#dvsliderPostLogin').css('display', 'none');
            $('#offerStatement').css('display', 'none');
        }


    } catch (e) {

    }
    $('.page').css('display', 'none').removeClass('current_page');
    $('#' + page).css('display', 'block').addClass('current_page');

}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {

        return false;
    }

    return true;
}

function Numeric(mobileNo) {
    var x = mobileNo.value;

    if (x.length < 10 || x.length > 11) {
        mobileNo.value = "";
        mobileNo.focus;
        swal({
            title: "Please enter valid mobile number !",
            type: "error",
        });
        return false
    }

}

function showPosition(position) {

    if ($("#ApMacId").val() !== "") {
        $("#longitude").val(position.coords.longitude);
        $("#lattitude").val(position.coords.latitude);
        HitsCount("Welcome");
    }
}
function showError(error) {
    HitsCount("Welcome");
}

function HitsCount(EventName) {

    var url = $('#UrlHomeHitCount').data('url');

    var data = {};

    data.Event = EventName;
    data.rId = $("#hdnResellerID").val() != null ? $("#hdnResellerID").val() : "";
    data.PId = $("#hdnPartnerID").val() != null ? $("#hdnPartnerID").val() : "";
    data.Mac = $("#ApMacId").val() != null ? $("#ApMacId").val() : "";
    data.Long = $("#longitude").val() != null ? $("#longitude").val() : "";
    data.Latt = $("#lattitude").val() != null ? $("#lattitude").val() : "";
    data.hID = $("#hotspotID").val() != null ? $("#hotspotID").val() : "";

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (msg) {

        }
    });

    return false;
}

function GetApDetail() {
    var url = $('#UrlHomeGetApDetail').data('url');
    var data = {};

    //if ($("#ApMacId").val() != '') {
    //    data.ApMac = $("#ApMacId").val();
    //    $.ajax({
    //        type: "POST",
    //        url: url,
    //        data: data,
    //        success: function (r) {
    //            if (r.Response == "Success") {
    //                $("#hdnOrgID").val(r.OrgId);
    //                $("#hotspotID").val(r.ID);
    //                $("#hIDforAdvertise").val(r.hid);
    //                $("#Locid").val(r.LocationID);



    //                //if (getCookie('SpidioValue') == "1") {

    //    $("#cpusername").val(getCookie("username"));
    //    $("#cppassword").val(getCookie("password"));
    //    $("#hdncustid").val(getCookie("custid"));
    //    $("#hdnspidioplanid").val(getCookie("spidioplanid"));
    //    $("#hdnhotspotgroup").val(getCookie('spidiorealm'));
    //    if (getCookie('RealmUserAvaiable') != null && getCookie('RealmUserAvaiable') == "No") {
    //        $('#otpevent').val('spidioregistration');
    //        doRegister();
    //    }
    //    else {
    //        Doaddon();
    //    }
    //}


    //                if (r.Hotspotgroup != null && r.Hotspotgroup != "") {
    //                    $("#hdnhotspotgroup").val("@" + r.Hotspotgroup);
    //                    $("#isEnterpriseuser").val("1");
    //                }
    //                else {
    //                    $('#btnViewPlan').css('display', 'block');
    //                    $('#btnPaidPlan').css('display', 'block');
    //                    $('#btnGoVoucherPlan').css('display', 'block');
    //                    CheckSpidioAdavaiablity();
    //                }
    //                var obj = { CPDesignID: r.CpDesignID }
    //                GetCPConfiguration(obj);
    //                GetPlanList();
    //                GetAdsList();
    //            }
    //            else {
    //                try {
    //                    console.log(r.Response)
    //                } catch (e) {

    //                }
    //            }

    //        },
    //        error: function (r) {
    //            try {
    //                console.log(r.Response)
    //            } catch (e) {

    //            }
    //        }
    //    });
    //}


    if ($('#SSIDMsg').val() != "") {
        swal({
            title: " " + $('#SSIDMsg').val(),
            type: "error",
        });

        $('#btnLogin').hide();
    }
    else {

        $('#btnLogin').show();

        if ($("#hdnhotspotgroup").val() == "") {
            $('#btnViewPlan').css('display', 'block');
            $('#btnPaidPlan').css('display', 'block');
            // $('#btnGoVoucherPlan').css('display', 'block');
            //CheckSpidioAdavaiablity();
        }

        var obj = { CPDesignID: $("#hdnCpDesignID").val() }
        GetCPConfiguration(obj);
        GetPlanList();
        if ($("#hndcontroller").val() != null && $("#hndcontroller").val() != "" && $("#hndcontroller").val() == "scintegration.js") {
            GetDecryptMac();
        }
    }
    // GetAdsList();
    return false;
}

function CheckSpidioAdavaiablity() {
    var url = $('#UrlHomeCheckSpidioAdAvailability').data('url');
    var data = {};
    data.Hid = $("#hotspotID").val();
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {

            var msg = r.Response;
            if (msg == "Success") {
                document.getElementById('dvvideolink').style.display = 'block';
            }
            else {
                document.getElementById('dvvideolink').style.display = 'none';
            }
        },
        error: function (r) {

            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        },
        failure: function (r) {

            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        }

    });
    return false;
}

function GetCPConfiguration(obj) {
    document.getElementById('linkSurfURL1').style.visibility = 'hidden';
    CheckHotspotAvaiable();
    var url = $('#UrlHomeGetCPConfiguration').data('url');
    var data = {};

    data.CPDesignID = obj.CPDesignID;

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {

            if (r.Response == "Success") {
                setCookie('linkSurfURL', "", 1);
                if (r.RedirectionURl != undefined && r.RedirectionURl != null && r.RedirectionURl != '') {
                    $('#linkSurfURL').attr("href", r.RedirectionURl);
                    setCookie('linkSurfURL', r.RedirectionURl, 1);
                    $('#linkSurfURL1').attr("href", r.RedirectionURl);
                    document.getElementById('linkSurfURL1').style.visibility = 'visible';
                }
                if (r.ISHybridPlanEnable != undefined && r.ISHybridPlanEnable != null && r.ISHybridPlanEnable == true) {
                    $('#hdnhybridplanenable').val("1");
                }

                if (r.isFPEnable != undefined && r.isFPEnable != null && r.isFPEnable == true) {
                    $('#forgotpass').css('display', 'inline-block');
                }


                if (r.IsAutoRegEnable != undefined && r.IsAutoRegEnable != null && r.IsAutoRegEnable == true)
                    $('#hdnautoregenable').val("1");
            }
            else {
                try {
                    console.log(r.Response)
                } catch (e) {

                }
            }

        },
        error: function (r) {
            try {
                console.log(r.Response)
            } catch (e) {

            }
        }
    });

    return false;
}

function OnlineRegistration(obj) {
    $('#otpevent').val('onlineregistration');
    $('#paidplanid').val(obj.planid);
    CheckCustomer();
    return false;
}


function AskToUser() {

    $('#otpevent').val('spidioregistration');

    $("#hdnuserclickonspidiolink").val(1);
    setCookie('SpidioValue', $("#hdnuserclickonspidiolink").val(), 1);

    if ($("#cpusername").val() == "") {
        swal({
            title: "Please enter mobile number !",
            type: "warning",
        });
        return false;
    }

    CheckCustomer();

    //    swal({
    //        title: "Are you alerdy  registered user?",
    //        //text: "You will not be able to recover this imaginary file!",
    //        type: "warning",
    //        showCancelButton: true,
    //        confirmButtonColor: "#DD6B55",
    //        confirmButtonText: "Yes, I am.",
    //        cancelButtonText: "No, I am not.",
    //        closeOnConfirm: false,
    //        closeOnCancel: false
    //    },
    //function (isConfirm) {
    //    if (isConfirm) {
    //        swal("Login first", "Enter your username and password for login", "success")
    //        $("#rRegisteredUser").prop("checked", true);
    //        $("#dvRegisteredUser").show();
    //        $("#dvNewUserRegistration").hide();
    //        setCookie('ClickOnValue', 'Login');
    //        // GetSpidioScript();

    //    } else {
    //        swal("Register first", "Fill your data and continue :)", "success");
    //        $("#rNewUserRegistration").prop("checked", true);
    //        $("#dvRegisteredUser").hide();
    //        $("#dvNewUserRegistration").show();
    //        setCookie('ClickOnValue', 'Register');
    //        // GetSpidioScript();
    //    }
    //});
    return false;
}


function GetAdsList() {
    var url = $('#UrlHomeGetAdsList').data('url');
    var data = {};
    data.OrgID = $("#hdnOrgID").val();
    data.HotspotId = $("#hIDforAdvertise").val();
    data.LocationId = $("#Locid").val();

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {
            if (r.Response == "Success") {
                for (i = 0  ; r.AdsList.length > i; i++) {

                    var htmlData = "";
                    var formtId = r.AdsList[i].DOC_FORMATE;

                    if (formtId == "291" || formtId == "11601") {
                        htmlData = (r.AdsList[i].HTML_HEADER != null ? r.AdsList[i].HTML_HEADER.toString() : "") + (r.AdsList[i].HTML_BODY != null ? r.AdsList[i].HTML_BODY.toString() : "") + (r.AdsList[i].HTML_FOOTER != null ? r.AdsList[i].HTML_FOOTER.toString() : "");
                    }
                    else {
                        if (formtId == "289") {
                            htmlData = '<a href="' + (r.AdsList[i].REDIRECT_LINK != null ? r.AdsList[i].REDIRECT_LINK.toString() : "") + '" target="_blank" title="' + (r.AdsList[i].DISPLAY_LINK != null ? r.AdsList[i].DISPLAY_LINK.toString() : "") + '"><img src="' + $('#hndPortalURL').val() + '/DocsMgmt/AdvertisementDocs/' + r.AdsList[i].FOLDER_NAME + '/' + r.AdsList[i].DOC_NAME + '"  alt="Advertisement" class="img-responsive"/></a>';
                        }
                        else if (formtId == "292") {
                            htmlData = '<video controls autoplay><source data-src="' + $('#hndPortalURL').val() + "/DocsMgmt/AdvertisementDocs/" + r.AdsList[i].FOLDER_NAME + '/' + r.AdsList[i].DOC_NAME + '" type="video/ogg"> </video>';
                        }
                    }

                    if (r.AdsList[i].LOCATION == 'Center') {
                        $('#dvslider').html(htmlData);
                    }
                    else if (r.AdsList[i].LOCATION == 'Center_Postlogin') {
                        $('#dvsliderPostLogin').html(htmlData);
                    }
                    else if (r.AdsList[i].LOCATION == 'interstitial_Prelogin') {
                        document.getElementById('insadd').style.display = 'block';
                        document.getElementById('insadd').innerHTML = htmlData;
                        var ads = $('#insadd').html();
                        $('#insadd').html(ads);

                    }
                    else if (r.AdsList[i].LOCATION == 'interstitial_Postlogin') {
                        $('#hdnpostaddintr').val(htmlData);
                    }
                }
            }
            else {
                try {
                    console.log(r.Response)
                } catch (e) {

                }
            }

        },
        error: function (r) {
            try {
                console.log(r.Response)
            } catch (e) {

            }
        }
    });


}

function GetPlanList(obj) {

    var div = 'dvViewPlan';
    if (obj != null && obj != undefined) {
        if (obj.div != undefined) {
            div = obj.div;
        }
    }
    var isPaid = false;
    if (div == 'dvPaidPlan') {
        isPaid = true;
    }

    $('#' + div).html("");
    var url = $('#UrlHomeGetPlanList').data('url');
    var data = {};

    data.HotspotId = $("#hotspotID").val() != null ? $("#hotspotID").val() : "";
    data.SSIDconfig = $("#SSIDconfig").val() != null ? $("#SSIDconfig").val() : "";



    //if ($('#' + div).children().length != 0) {
    //    return false;
    //}

    var event = "";
    if ($('#cppassword').val() != "") {

        event = 'RenewOnline';
    }
    else {
        event = 'OnlineRegistration';
    }


    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {

            if (r.Response == "Success") {
                for (i = 0  ; r.PaidPlanList.length > i; i++) {

                    $('#' + div).append('<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 plan_container" ' + (isPaid ? 'style="cursor: pointer;"' : '') + '  ' + (isPaid ? 'onclick="javascript: return ' + event + '({planid : ' + r.PaidPlanList[i].ID + '});"' : '') + ' > ' +
                    '<div class="bg-warning">' +
                        '<div class="prize">' +
                         '   <img src="' + $('#hnImagepath').val() + '" />' +
                         '   <h2>' + r.PaidPlanList[i].MRP + '</h2>' +
                       ' </div>' +
                       ' <div class="plan">' +
                        '    <h3>' + r.PaidPlanList[i].USAGEDATA + '</h3>' +
                         '   <h5>' + r.PaidPlanList[i].VALIDITY + '</h5>' +
                       ' </div>' +
                   ' </div> ' +
               ' </div>');

                }

                $('#hdnfreeplanid').val(r.FreePlanID);
            }
            else {
                try {
                    console.log(r.Response)
                } catch (e) {

                }
            }

        },
        error: function (r) {
            try {
                console.log(r.Response)
            } catch (e) {

            }
        }
    });

    return false;
}

function GetCountryList(obj) {

    if (obj.length > 1) {
        return false;
    }

    var url = $('#UrlHomeGetCountryList').data('url');
    var data = {};

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {

            if (r.Response == "Success") {

                $("#" + obj.id + "").children().remove();

                for (i = 0  ; r.CountryList.length > i; i++) {
                    $("#" + obj.id + "").get(0).options[$("#" + obj.id + "").get(0).options.length] = new Option(r.CountryList[i].Name, r.CountryList[i].Code);
                }

            }
            else {
                try {
                    console.log(r.Response)
                } catch (e) {

                }
            }

        },
        error: function (r) {
            try {
                console.log(r.Response)
            } catch (e) {

            }
        }
    });

    return false;
}



//function CheckRetailuserexist() {
//    var url = $('#UrlHomeCheckUserWithSameMobilenumber').data('url');

//    var data = {};
//    data.mobileNo = $("#loginCountry").val() + $("#cpusername").val();

//    $.ajax({
//        type: "POST",
//        url: url,
//        data: data,
//        success: function (r) {

//            var msg = r.Response;
//            if (msg == "Retail" || msg == "Enterprise") {
//                $("#isRetailuserfound").val(1);
//                $("#Sendexistingpassword").val(r.Id);
//                swal({
//                    title: "You are already registered user,do Login for browsing.",
//                    type: "error",
//                });
//                return false;
//            }
//            else {
//                $("#Sendexistingpassword").val(0);
//                $("#hdnuserexists").val(1);
//                getOtp();
//            }

//        },
//        error: function (r) {
//            var msg = r.Response;
//            swal({
//                title: " " + msg,
//                type: "error",
//            });
//        },
//        failure: function (r) {
//            var msg = r.Response;
//            swal({
//                title: " " + msg,
//                type: "error",
//            });
//        }
//    });
//    return false;
//}


function CheckCustomer() {

    CheckHotspotAvaiable();
    if ($("#cpusername").val() == "") {
        swal({
            title: "Please enter username !",
            type: "warning",
        });
        return false;
    }
    if ($("#cppassword").val() == "") {
        swal({
            title: "Please enter Password !",
            type: "warning",
        });
        return false;
    }
    var url = $('#UrlHomeCheckCustomerStatus').data('url');

    var data = {};

    data.mobileNo = $("#cpusername").val();
    data.otp = $("#cppassword").val();
    data.LocationId = $("#Locid").val() != null ? $("#Locid").val() : "";
    data.mac = $("#ApMacId").val() != null ? $("#ApMacId").val() : "";
    data.hotspotgroup = $("#hdnhotspotgroup").val() != null ? $("#hdnhotspotgroup").val() : "";
    data.client_mac = $("#hdnMACAddressDec").val() != null ? $("#hdnMACAddressDec").val() : "";
    data.chkexistuser = "1";

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {

            var msg = r.Response;

            if ((msg == "Invalid Username or Password!" || msg == "New - Activation Pending") && $("#isEnterpriseuser").val() == "1" && $("#hdnhybridplanenable").val() == "0" && $("#hdnautoregenable").val() == "0") {
                CheckPreRegisteredUser();
                return false;
            }


            if ($("#isEnterpriseuser").val() == "1") {

                if (msg == "New - Activation Pending") {
                    FillDetail();
                    return false;
                }
                //else if ($("#hdnhybridplanenable").val() == "0" && $("#hdnautoregenable").val() == "0") {
                //    swal({
                //        title: "You are not a Pre registered User, Please contact Administrator.",
                //        type: "error",
                //    });
                //    return false
                //}
            }

            if (msg != "Invalid Username or Password!") {

                if (r.client_mac_exist) {
                    $("#cppassword").val(r.password);
                }

                if ($("#cppassword").val() != '') {
                    doLogin();
                }
                else {

                    getPassward();

                    ChangeUrl('otp');
                    $('#divForgotOTP').css('display', 'block');
                    $('#divResendOTP').css('display', 'none');
                    $('#otpevent').val('login');
                }

                return false;
            }
            else if (r.userexist == "User Does not exists !" || r.userexist == null || r.userexist == "null" || r.userexist == "") {
                if (r.userexist == "User Does not exists !" || r.userexist == "UserNotExist" || r.userexist == null || r.userexist == "") {
                    swal({
                        title: "Invalid Username or Password!",
                        type: "warning",
                    });
                    return false;
                }
                var obj = { otpevent: 'freeregistration' };

                //if ($("#hdnhotspotgroup").val() != "") {
                //    swal({
                //        title: "You are not a Pre registered User, Please contact Administrator.",
                //        type: "warning",
                //    });
                //    return false;
                //}
                //else
                if ($('#otpevent').val() == 'onlineregistration') {

                    obj = { otpevent: $('#otpevent').val(), planid: $('#paidplanid').val() };
                }
                else if ($('#otpevent').val() == 'voucherregistration') {
                    obj = { otpevent: $('#otpevent').val() };
                }
                //else if ($('#otpevent').val() == 'spidioregistration') {

                //    obj = { otpevent: $('#otpevent').val() };
                //    GetSpidioScript();
                //    return false
                //}

                getOtp(obj);
                $('#divForgotOTP').css('display', 'none');
                $('#divResendOTP').css('display', 'block')

                return false;
            }
            else if (r.Validuser == true && $("#hdnautoregenable").val() == "1") {
                //If Retail OR Enterprise User Found then SEND OTP As per Userid's password & Create user with Current hotspot group              

                var obj = { otpevent: 'autoregistration' };

                $('#Sendexistingpassword').val(r.custid);

                getOtp(obj);
                $('#divForgotOTP').css('display', 'none');
                $('#divResendOTP').css('display', 'block');
            }
            else {

                var obj = { otpevent: 'freeregistration' };
                getOtp(obj);
                $('#divForgotOTP').css('display', 'none');
                $('#divResendOTP').css('display', 'block');
                return false;
            }

        }
    });
    return false;
}


function getOtp(obj) {
    CheckHotspotAvaiable();
    if ($("#mobileNo").val() == "") {
        swal({
            title: "Please enter mobile number !",
            type: "warning",
        });
        return false;
    }

    var url = $('#UrlHomeGenerateOTP').data('url');
    var data = {};

    data.mobileNo = $("#loginCountry").val() + $("#cpusername").val();
    data.emailId = "";
    data.name = "";
    data.planid = 0,
    data.isExistUser = true;
    data.GetOTPPayOnline = false
    data.GetOTPlogin = false;
    data.PartnerID = $("#hdnPartnerID").val() != null ? $("#hdnPartnerID").val() : 0;
    data.resellerId = $("#hdnResellerID").val() != null ? $("#hdnResellerID").val() : 0;
    data.hotspot = $("#hotspotID").val() != null ? $("#hotspotID").val() : 0;
    data.hotspotgroup = $("#hdnhotspotgroup").val() != null ? $("#hdnhotspotgroup").val() : "";
    data.Sendexistingpassword = $("#Sendexistingpassword").val();

    if (obj != null && obj != undefined) {
        if (obj.otpevent != undefined && (obj.otpevent == 'freeregistration' || obj.otpevent == 'voucherregistration' || obj.otpevent == 'onlineregistration') || obj.otpevent == 'enterpriseregistration' || obj.otpevent == 'spidioregistration' || obj.otpevent == 'autoregistration') {
            data.isExistUser = false;
            $('#otpevent').val(obj.otpevent);

            if (obj.otpevent == 'voucherregistration') {
                url = $('#UrlHomeGenerateOTPForVoucherRegistration').data('url');
                data.vPin = $("#vPin").val();
                data.vPassword = "";
            }
            else if (obj.otpevent == 'onlineregistration') {
                url = $('#UrlHomeGenerateOTPForOnlinePayRegistration').data('url');
                data.planid = obj.planid;
                $('#paidplanid').val(obj.planid)
                data.GetOTPPayOnline = true;
            }
            else if (obj.otpevent == 'enterpriseregistration') {
                data.isExistUser = true;
            }
            else if (obj.otpevent == 'spidioregistration') {
                data.planid = getCookie('spidioregplanid');
            }

        }
    }


    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {

            var msg = r.Response;

            if (msg == "Success") {

                $("#cppassword").val("");
                ChangeUrl('otp');

                $('#xxxMobile').html($("#cpusername").val());
                try {
                    $('#cppassword').focus();
                } catch (e) {
                    console.log(e.message);
                }
            } else {

                swal({
                    title: " " + msg,
                    type: "warning",
                });
            }
        },
        error: function (r) {
            swal({
                title: " " + msg,
                type: "warning",
            });
        },
        failure: function (r) {
            swal({
                title: " " + msg,
                type: "warning",
            });
        }
    });
    return false;
}

function OTPVerfication() {

    if ($("#cppassword").val() == "") {
        swal({
            title: "Enter OTP",
            type: "warning",
        });
        return false;
    }

    if ($('#otpevent').val() == 'login') {
        doLogin();
        return false;
    }


    var url = $('#UrlHomeOTPVerification').data('url');
    var data = {};
    data.mobileNo = $("#loginCountry").val() + $("#cpusername").val();
    data.OTP = $("#cppassword").val();



    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {

            var msg = r.Response;
            if (msg == "Success") {
                if ($('#otpevent').val() == 'freeregistration' || $('#otpevent').val() == 'voucherregistration' || $('#otpevent').val() == 'onlineregistration' || $('#otpevent').val() == 'enterpriseregistration' || $('#otpevent').val() == 'spidioregistration' || $('#otpevent').val() == 'autoregistration') {
                    doRegister();
                    return false;
                }
            }
            else {
                swal({
                    title: " " + msg,
                    type: "warning",
                });
            }
        }
    });
    return false;

}


function doRegister() {
    CheckHotspotAvaiable();
    var url = $('#UrlHomeWiFiRegistration').data('url');
    var data = {};

    data.mobileNo = $("#loginCountry").val() + $("#cpusername").val();
    data.name = "";
    data.emailID = "";
    data.country = $("#loginCountry option:selected").text();
    data.password = $("#cppassword").val();
    data.mac = $("#ApMacId").val() != null ? $("#ApMacId").val() : "";
    data.rId = $("#hdnResellerID").val() != null ? $("#hdnResellerID").val() : "";
    data.hId = $("#hotspotID").val() != null ? $("#hotspotID").val() : "";

    data.SSIDconfig = $("#SSIDconfig").val() != null ? $("#SSIDconfig").val() : "";

    data.PId = $("#hdnPartnerID").val() != null ? $("#hdnPartnerID").val() : "";
    data.locationid = $("#Locid").val() != null ? $("#Locid").val() : "";
    data.isExistUser = false;
    data.hotspotgroup = $("#hdnhotspotgroup").val() != null ? $("#hdnhotspotgroup").val() : "";

    if ($('#otpevent').val() == 'voucherregistration') {
        url = $('#UrlHomeRegistrationWithVoucher').data('url');
        data.vPin = $("#vPin").val();
        data.vPassword = "";
    }
    else if ($('#otpevent').val() == 'onlineregistration') {
        url = $('#UrlHomeRegistrationWithOnlinePay').data('url');
        data.PlanId = $("#paidplanid").val();
    }
    else if ($('#otpevent').val() == 'enterpriseregistration') {
        url = $('#UrlHomeExistBulkUploadUserupdate').data('url');
        data.isExistUser = true;
    }
    else if ($('#otpevent').val() == 'spidioregistration') {
        url = $('#UrlHomeWiFiRegistrationSpidio').data('url');
        data.PlanId = getCookie('spidioregplanid');
    }


    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {
            var msg = r.Response;
            if (msg == "Success") {
                swal({
                    title: "Registration successfull.!",
                    type: "success",
                });

                setCookie('username', $("#cpusername").val(), 1);
                setCookie('password', $("#cppassword").val(), 1);
                setCookie('custid', r.custid);


                if ($('#otpevent').val() == 'onlineregistration') {
                    $("#custStatus").val("Trial");
                    $("#onlinePurchaseReciept").val(r.reqid);
                    $("#isEnterpriseuser").val(0);
                    $("#hdnhotspotgroup").val("");
                }
                //else {
                //    Doaddon();
                //    return false;
                //}

                doLogin();

            } else {
                swal({
                    title: " " + msg,
                    type: "error",
                });
            }
            //HitsCount("Register");
        },
        error: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        },
        failure: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        }
    });

    return false;
}

//function Doaddon() {

//    $("#hdncustid").val(getCookie("custid"));
//    var url = $('#UrlHomeDoaddonplan').data('url');
//    var data = {};
//    data.custid = $("#hdncustid").val() != null ? $("#hdncustid").val() : "";
//    data.planid = ($("#hdnspidioplanid").val() != null ? $("#hdnspidioplanid").val() : "");

//    $.ajax({
//        type: "POST",
//        url: url,
//        data: data,
//        success: function (r) {
//            var msg = r.Response;
//            if (msg == "Success") {
//                swal({
//                    title: "Free data addedd successfully",
//                    type: "success"
//                });
//                $("#cpusername").val(getCookie("username"));
//                $("#cppassword").val(getCookie("password"));
//                setCookie('SpidioValue', $("#hdnuserclickonspidiolink").val(0), 1);
//                doLogin();
//                return false;
//            }
//            else if (msg == "Failed") {
//                setCookie('SpidioValue', $("#hdnuserclickonspidiolink").val(0), 1);
//                return false;
//            }
//        },
//        error: function (r) {
//            var msg = r.Response;
//            swal({
//                title: " " + msg,
//                type: "error",
//            });
//            setCookie('SpidioValue', $("#hdnuserclickonspidiolink").val(0), 1);
//        },
//        failure: function (r) {
//            var msg = r.Response;
//            swal({
//                title: " " + msg,
//                type: "error",
//            });
//            setCookie('SpidioValue', $("#hdnuserclickonspidiolink").val(0), 1);
//        }
//    });
//    return false;
//}

function FillDetail() {

    var url = $('#UrlHomeGetCustomerDetail').data('url');
    var data = {};
    data.mobileNo = $("#loginCountry").val() + $("#cpusername").val();
    data.hotspotgroup = $("#hdnhotspotgroup").val();

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {
            var msg = r.Response;
            if (msg == "Success") {
                $("#Sendexistingpassword").val(r.Id);
                var obj = { otpevent: 'enterpriseregistration' };
                getOtp(obj);
                return false;
            }

        },
        error: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        },
        failure: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        }
    });
    return false;
}

function GetUserByrequestId() {

    var url = $('#UrlHomeGetUserByRequestId').data('url');
    var data = {};
    data.RequestID = $('#hndReqId').val();

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {
            var msg = r.Response;
            if (msg == "Success") {
                $("#cpusername").val(r.mobileNo.substr(r.mobileNo.length - 10));
                $("#loginCountry").val(r.mobileNo.substring(0, r.mobileNo.length - 10));
                $("#cppassword").val(r.Password);
                openLogin();
                doLogin();
            }
        },
        error: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        },
        failure: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        }

    });


    return false;
}
function doLogin() {
    CheckHotspotAvaiable();
    HitsCount("Login");

    if ($("#cpusername").val() == "") {
        swal({
            title: "Please enter mobile number !",
            type: "warning",
        });
        return false;
    }
    if ($("#cppassword").val() == "") {
        swal({
            title: "Enter PIN",
            type: "warning",
        });

        return false;
    }

    $("#btnLogin").attr("disabled", "disabled");
    $("#cpusername").attr("disabled", "disabled");
    $("#cppassword").attr("disabled", "disabled");
    $("#btnLogin").val("Please Wait...");
    $("#btnLogin").attr("enable", "enable");
    $("#uname").html($("#cpusername").val() + $("#hdnhotspotgroup").val());

    var url = $('#UrlHomeCheckCustomerStatus').data('url');

    var data = {};

    data.mobileNo = $("#cpusername").val();
    data.otp = $("#cppassword").val();
    data.LocationId = $("#Locid").val() != null ? $("#Locid").val() : "";
    data.mac = $("#ApMacId").val() != null ? $("#ApMacId").val() : "";
    data.hotspotgroup = $("#hdnhotspotgroup").val() != null ? $("#hdnhotspotgroup").val() : "";
    data.client_mac = $("#hdnMACAddressDec").val() != null ? $("#hdnMACAddressDec").val() : "";
    data.chkexistuser = "0";


    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {
            var msg = r.Response;
            setCookie('username', $("#cpusername").val(), 1);
            setCookie('password', $("#cppassword").val(), 1);
            setCookie('custid', r.custid, 1);
            setCookie('hotspotgroup', $("#hdnhotspotgroup").val(), 30);
            setCookie('hid', $("#hotspotID").val(), 30);
            if (msg == "Inactive") {

                if ($("#isEnterpriseuser").val() == "1" && $("#hdnhybridplanenable").val() == "1") {

                    var obj = {};
                    obj.status = msg;
                    GetPaidPlanDetails(obj);
                    return false;
                }
                //if ($("#hdnuserclickonspidiolink").val() == 1) {
                //    GetSpidioScript();
                //    return false;
                //}
                if ($("#hdnhotspotgroup").val() == "" && $("#hdnchkfreeplan").val() == "0") {
                    CheckFreePlanRenewal();
                    return false;
                }
                if ($("#hdnhotspotgroup").val() == "" || $("#hdnhybridplanenable").val() == "1") {
swal({
					  title: 'Your Current Plan is Expired',
					  text: 'You are eligible for 10 Min free plan to purchase paid one(Click on YES).',
					  type: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#DD6B55',
					  confirmButtonText: 'Yes!',
					  cancelButtonText: 'No.'
					  }).then(function () {
						  var obj = { planid: $("#PlanIdForTrial").val() }
                        // var obj = { planid: $("#PlanIdForTrial").val() }
                                  RenewOnline(obj);
								  if(obj.Response=="Success")
								  {
								  Authenticate();
								  
								  
								  }								 
                                   
                                    return false;
								  
                        //alert("The confirm button was clicked");
                    }).catch(function (reason) {
                        alert("The alert was dismissed by the user: " + reason);
                    });
					  
                    // swal(
                     // {
                         // title: "Your plan is expired.", text: "Do you want to recharge ? ",
                         // showConfirmButton: true, showCancelButton: true, closeOnConfirm: true, showLoaderOnConfirm: true
                     // },
                     // function () {
                         // OpenRenewbox();
                     // }
                     // );
                   // $("#btnLogin").attr("enable", true);
                    return false;
                }
                else {
                    if ($("#isEnterpriseuser").val() == "1" && $("#hdnhybridplanenable").val() == "0") {
                        swal({
                            title: "Your Quota is completed for the day!",
                            type: "error",
                        });
                        return false;
                    }
                    else {
                    }
                    swal({
                        title: "You are not able to login, please contact to administrator!",
                        type: "error",
                    });
                }
            }

            else if (msg == "Terminated") {
                swal({
                    title: " Your account is terminated.",
                    type: "warning",
                });
                $("#cpusername").removeAttr("disabled");
                $("#cppassword").removeAttr("disabled");
                $("#btnLogin").removeAttr("disabled");
                $("#btnLogin").val("Login");
                return false;
            }
            else if (msg == "Suspended") {
                swal({
                    title: "Your account is suspended.",
                    type: "warning",
                });
                $("#cpusername").removeAttr("disabled");
                $("#cppassword").removeAttr("disabled");
                $("#btnLogin").removeAttr("disabled");
                $("#btnLogin").val("Login");
                return false;
            }
            else if (msg == "Active" || msg == "Trial") {
				
                    
                try {
                    if ($("#isEnterpriseuser").val() == "1" && $("#hdnhybridplanenable").val() == "1") {
                        var obj = {};
                        obj.status = msg;
                        GetPaidPlanDetails(obj);
                        return false;
                    }
                    Authenticate();
                } catch (e) {
                    $("#cpusername").removeAttr("disabled");
                    $("#cppassword").removeAttr("disabled");
                    $("#btnLogin").removeAttr("disabled");
                    $("#btnLogin").val("Login");
                }

                return false;
            }

            else {
                swal({
                    title: " " + msg,
                    type: "error",
                });
                $("#cpusername").removeAttr("disabled");
                $("#cppassword").removeAttr("disabled");
                $("#btnLogin").removeAttr("disabled");
                $("#btnLogin").val("Login");
                return false;
            }
        },
        error: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        },
        failure: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        }

    });

    $("#btnLogin").attr("enable", "enable");
    return false;
}



function openStatus() {

    HitsCount("OpenStatus");
    ChangeUrl('status');

    return false;
}
function openSessionSummary() {
    ChangeUrl('logout');
    return false;
}

function openLogin() {
    HitsCount("OpenLogin");
    ChangeUrl('login');
    return false;
}

function OpenRenewbox() {
    var obj = { div: 'dvPaidPlan' }
    GetPlanList(obj);
    ChangeUrl('paidplan');
    return false;
}

function GetSpidioScript() {

    var url = $('#UrlHomeSendSpidioAdScript').data('url');
    var data = {};
    data.Hid = $("#hotspotID").val();
    data.Custid = $("#hdncustid").val() != null ? $("#hdncustid").val() : "";
    data.MobileNo = $("#loginCountry").val() + $("#cpusername").val();

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {

            var msg = r.Response;
            if (msg == "Success") {
                setCookie('quotafor', r.QuotaFor);
                setCookie('spidiorealm', r.Realmvalue);
                setCookie('spidioplanid', r.PlanId);
                setCookie('spidioregplanid', r.AdvRegPlanId);
                setCookie('custid', r.CustId);
                setCookie('RealmUserAvaiable', r.RealmUserAvaiable);
                document.getElementById('insadd').style.display = 'block';
                document.getElementById('insadd').innerHTML = r.ScriptData;
                setCookie('ChkSpidioOption', 1);
                var div = $('#insadd').html();
                $('#insadd').html(div);
                return false;
            }
            else if (msg == "Not allowed") {
                $("#hdnuserclickonspidiolink").val(0);
                setCookie('SpidioValue', $("#hdnuserclickonspidiolink").val(0), 1);
                setCookie('ChkSpidioOption', 0);
                swal({
                    title: "Sorry! You have already used video advertisement benefits.",
                    type: "warning",
                });
                return false;
            }
            else if (msg == "Failed") {
                $("#hdnuserclickonspidiolink").val(0);
                setCookie('SpidioValue', $("#hdnuserclickonspidiolink").val(0), 1);
                setCookie('ChkSpidioOption', 0);
                return false;
            }
        },
        error: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        },
        failure: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        }
    });
    return false;
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function mserveCallback(e) {

    location.reload();
    return false;
}

function GetPaidPlanDetails(obj) {

    var url = $('#UrlHomeGetPaidPlanDetailsOfRetailCustomer').data('url');

    var data = {};

    data.mobileNo = $("#loginCountry").val() + $("#cpusername").val();


    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {

            var msg = r.Response;

            if (msg == "Success") {

                $("#isRetailuserfound").val(1);
                var userid = r.Userid;
                var password = r.Password;
                var userstatus = r.Status.toString();
                password = password.toString();
                $("#cppassword").val(password);

                $('#txtPaidPlanMRP').html('₹ ' + r.MRP);
                $('#txtPaidPlanTotalData').html(r.TotalData + ' MB');
                $('#txtPaidPlanValidity').html(r.Validity);
                $('#txtPaidPlanDataBalance').html(r.BalanceData + ' MB');
                $('#txtPaidPlanExpiry').html(r.ExpiryData);


                $("#choosepaidplan").click(function () {
                    return BrowsingOption(userid, password, 1, userstatus);
                });
                $("#choosecurrentplan").click(function () {
                    return BrowsingOption(userid, password, 0, userstatus);
                });

                ChangeUrl('hybridplan');

                return false;
            }
            else if (msg == "Retailuserexist") {

                if (r.Status == "Active" && obj != null && (obj.status.toString() != "Active" && obj.status.toString() != "Trial")) {
                    swal({
                        title: "Your Quota is completed for the day!",
                        type: "error",
                    });
                    return false;
                }
                else {
                    $("#isEnterpriseuser").val(0);
                    $("#isRetailuserfound").val(1);
                    doLogin();
                }
            }
            else if (msg == "Failed") {
                $("#isEnterpriseuser").val(0);
                $("#isRetailuserfound").val(0);
                doLogin();
            }
            else {
                swal({
                    title: "Your Quota is completed for the day!",
                    type: "error",
                });
                return false;
            }

        },
        error: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        },
        failure: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        }
    });
    return false;
}

function BrowsingOption(Userid, password, isretailplanuse, status) {

    $("#isEnterpriseuser").val(0);
    password = $("#cppassword").val();
    if (isretailplanuse == "1") {

        if (status == 'Active') {
            $("#isEnterpriseuser").val(0);
            $("#hdnhotspotgroup").val("");
            setCookie('hotspotgroup', '', 30);
            $("#IsPaidplanavailble").val(0);
            doLogin();
        }
        else {
            swal(
                    {
                        title: "Your plan is expired.", text: "Do you want to recharge ? ",
                        showConfirmButton: true, showCancelButton: true, closeOnConfirm: true, showLoaderOnConfirm: true
                    },
                    function () {
                        OpenRenewbox();
                    }
                    );
            return false;
        }
    }
    else {
        $("#IsPaidplanavailble").val(1);
        setCookie('hotspotgroup', $("#hdnhotspotgroup").val(), 30);
        var url = $('#UrlHomeCheckCustomerStatus').data('url');

        var data = {};

        data.mobileNo = $("#loginCountry").val() + $("#cpusername").val();
        data.otp = $("#cppassword").val();
        data.LocationId = $("#Locid").val() != null ? $("#Locid").val() : "";
        data.mac = $("#ApMacId").val() != null ? $("#ApMacId").val() : "";
        data.hotspotgroup = $("#hdnhotspotgroup").val() != null ? $("#hdnhotspotgroup").val() : "";
        data.client_mac = $("#hdnMACAddressDec").val() != null ? $("#hdnMACAddressDec").val() : "";
        data.chkexistuser = "0";

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (r) {
                var msg = r.Response;
                if (msg == "Inactive") {
                    swal({
                        title: "Your current plan is Inactive.", text: "Do you want to continue with paid plan ? ",
                        showConfirmButton: true, showCancelButton: true, closeOnConfirm: true, showLoaderOnConfirm: true
                    },
                        function () {
                            setTimeout(function () { BrowsingOption(Userid, password, "1", status); }, 1000);
                        }
                        );
                    return false;
                }
                else {
                    doLogin();
                }
            }
        });
    }
}








function CheckFreePlanRenewal() {
    CheckHotspotAvaiable();
    if ($("#hdnfreeplanid").val() == "0") {
        $("#hdnchkfreeplan").val(1);
        doLogin();
        return false;
    }
    else {

        var url = $('#UrlHomeFreePlanEligibility').data('url');
        var data = {};

        data.mobileNo = $("#cpusername").val();
        data.hotspotid = $("#hotspotID").val();
	if ($('#PlanIdForTrial').val() != null)
        {
            data.planid = $('#PlanIdForTrial').val();
        }
        else
        {
            data.planid = "";
        }
        //data.planid = $("#hdnfreeplanid").val();
        data.SSIDconfig = $("#SSIDconfig").val() != null ? $("#SSIDconfig").val() : "";


        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function (r) {

                var msg = r.Response;
                if (msg == "Success") {
					swal({
					  title: 'Your Current Plan is Expired.',
					  text: 'You are eligible for 10 Min free plan to purchase paid one(Click on YES).',
					  type: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#DD6B55',
					  confirmButtonText: 'Yes!',
					  cancelButtonText: 'No.'
					  }).then(function () {
						  var obj = { planid: $("#PlanIdForTrial").val(planid) }
                        // var obj = { planid: $("#PlanIdForTrial").val() }
                                  RenewOnline(obj);
								  if(obj.Response=="Success")
								  {
								  Authenticate();
								  
								  
								  }								 
                                   
                                    return false;
								  
                        //alert("The confirm button was clicked");
                    }).catch(function (reason) {
                        alert("The alert was dismissed by the user: " + reason);
                    });
					  
					  
					  
					  
					  
					  
					  
					// }).then((result) => {
						
					  // if (result) {
						   // var obj = { planid: $("#hdnfreeplanid").val() }
                           // RenewOnline(obj);
						   // window.location.replace('https://customer.i-on.in/');
					     // return false;
					  // } else {
						// // result.dismiss can be 'cancel', 'overlay', 'esc' or 'timer'
						// location.reload();

					  // }
					// });
					
					
					
					
					
                    // swal(
                                      // {
                                          // title: "Your Current Plan is Expired.", text: "You are eligible for 10 Min free plan to purchase paid one(Click on YES).", type: "warning",
                                          // showCancelButton: true,
                                          // confirmButtonText: "Yes",
                                          // cancelButtonText: "cancel",
                                          // closeOnConfirm: false,
                                          // closeOnCancel: false
                                      // },
                                         // function (isConfirm) {
                                             // if (isConfirm) {
                                                 // var obj = { planid: $("#hdnfreeplanid").val() }
                                                 // RenewOnline(obj);
                                                 // //$("#hdnchkfreeplan").val(1);
                                                 // // DoFreeRenewal();
                                                 // return false;
                                             // } else {
                                                 // window.location.href = System.Configuration.ConfigurationManager.AppSettings["CaptiveportalUrl"];
                                                 // return false;
                                             // }
                                             // return false;
                                         // });
                }
                else {
                    $("#hdnchkfreeplan").val(1);
                    doLogin();
                    return false;
                }
            },
            error: function (r) {
                var msg = r.Response;
                swal({
                    title: " " + msg,
                    type: "error",
                });
            },
            failure: function (r) {
                var msg = r.Response;
                swal({
                    title: " " + msg,
                    type: "error",
                });
            }
        });
        return false;
    }
}
function DoFreeRenewal() {
    CheckHotspotAvaiable();
    var url = $('#UrlHomeDoFreeRenewal').data('url');
    var data = {};
    data.mobileNo = $("#loginCountry").val() + $("#cpusername").val();
    data.hotspotID = $("#hotspotID").val();
    data.PlanId = $("#hdnfreeplanid").val();
    data.LocationId = $("#Locid").val() != null ? $("#Locid").val() : "";

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {
            var msg = r.Response;
            if (msg == "SUC") {
                $("#hdnchkfreeplan").val(1);
                doLogin();
                return false;
            }
            else {
                swal({
                    title: " " + msg,
                    type: "error",
                });
                return false;
            }
        },
        error: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        },
        failure: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        }
    });
    return false;
}


function CheckSpidioAvaiablePlan() {

    var url = $('#UrlHomeCheckSpidioPlanOption').data('url');
    var data = {};
    data.mobileNo = $("#loginCountry").val() + getCookie('username');
    data.HotspotGroup = $("#hdnhotspotgroup").val() != null ? $("#hdnhotspotgroup").val() : "";

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {
            var msg = r.Response;
            if (msg == "Success") {


                //var paiddata = "<a href='javascript:void(0)' onclick='javascript: return BrowsingForSpidio(1);'><div class='col-xs-6 block'><p class='circle'><Span style='color:black !important;'>Continue browsing with 15 Min plan<br /><br /></Span></p></div></a>"
                //document.getElementById('dvdynamicpaidplan').innerHTML = paiddata;

                //var currentdata = "<a href='javascript:void(0)' onclick='javascript: return BrowsingForSpidio(0);'><div class='col-xs-6 block'><p class='circle'><Span style='color:black !important;'>Start Free Browsing</Span></p></div></a>"
                //document.getElementById('dvdynamicCurrentplan').innerHTML = currentdata;
            }
            else {
                doLogin();
            }
        },
        error: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        },
        failure: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        }
    });
    return false;
}

function BrowsingForSpidio(BType) {
    $("#cpusername").val(getCookie('username'));
    $("#cppassword").val(getCookie('password'));
    setCookie('ChkSpidioOption', 0, 1);
    $("#hdnuserclickonspidiolink").val(0);

    if (BType == 1) {
        $("#hdnhotspotgroup").val(getCookie('spidiorealm'));
    }
    else {
        $("#hdnhotspotgroup").val("");
    }
    doLogin();
}

function ViewCurrentPlanDetails() {
    if (getCookie('hotspotgroup') != null && getCookie('hotspotgroup') != '' && getCookie('hotspotgroup') != "") {
        $("#hdnhotspotgroup").val(getCookie('hotspotgroup'));
    }
    else {
        $("#hdnhotspotgroup").val("");
    }
    var url = $('#UrlHomeViewCurrentPlanDetail').data('url');
    var data = {};
    data.mobileNo = $("#loginCountry").val() + $("#cpusername").val();
    data.hotspotgroup = $("#hdnhotspotgroup").val();


    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function (r) {

            var msg = r.Response;
            if (msg == "Success") {

                $('#spanUsername').html($("#cpusername").val());
                $('#spanDownloadData').html(r.UsedDownloadMB);
                $('#spanUploadData').html(r.UsedUploadMB);
                $('#spanPlanData').html(r.TotalMB);
                $('#spanAvailableData').html(r.RemainMB);
                $('#spanUsedData').html(r.UsedMB);

            }
        },
        error: function (r) {
            var msg = r.Response;
        },
        failure: function (r) {
            var msg = r.Response;
        }
    });

    return false;
}

function GetConcurrentSession() {

    var url = $('#UrlHomeGetConcurrentSession').data('url');
    var data = {};

    data.mobileNo = $("#loginCountry").val() + $("#cpusername").val();
    data.hotspotgroup = $("#hdnhotspotgroup").val();

    $.ajax({
        type: "POST",
        url: url,
        data: data
    })

.done(function (retStrings) {

    var msg = retStrings.Response;
    if (msg == "Success") {

        try {
            $('#spanFramIP').html(retStrings.FRAMED_IP_ADDRESS);
            $('#spanSessionTime').html(retStrings.ACCT_SESSION_TIME);
            $('#spanUploadData').html(retStrings.ACCT_INPUT_OCTETS);
            $('#spanDownloadData').html(retStrings.ACCT_OUTPUT_OCTETS);
            $('#spanUsedData').html(parseFloat($('#spanUsedData').html()) + parseFloat(retStrings.ACCT_INPUT_OCTETS) + parseFloat(retStrings.ACCT_OUTPUT_OCTETS));
            if ($('#spanAvailableData').html() != null && $('#spanAvailableData').html() != "" && $('#spanAvailableData').html().toString() != "Unlimited") {
                $('#spanAvailableData').html(parseFloat($('#spanAvailableData').html()) - (parseFloat(retStrings.ACCT_INPUT_OCTETS) + parseFloat(retStrings.ACCT_OUTPUT_OCTETS)));
            }
        } catch (e) {
            console.log("Error in getting concurrent session details", e.message);
        }
    }
    setTimeout(function () { GetConcurrentSession(); }, 120000);
});
    return false;
}

function getPassward() {
    CheckHotspotAvaiable();
    if ($("#cpusername").val() == "") {
        swal({
            title: "Please enter mobile number !",
            type: "warning",
        });

        return false;
    }

    var url = $('#UrlHomeResendOTPGeneration').data('url');
    var data = {};

    data.mobileNo = $("#loginCountry").val() + $("#cpusername").val()
    data.hId = $("#hotspotID").val() != null ? $("#hotspotID").val() : "";
    data.LocationId = $("#Locid").val() != null ? $("#Locid").val() : "";
    data.hotspotgroup = $("#hdnhotspotgroup").val() != null ? $("#hdnhotspotgroup").val() : "";

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {
            var msg = r.Response;

            if (msg == "SuccessM" || msg == "SuccessB" || msg == "SuccessE" || msg == "Success") {

                $('#btnForgotOTPVerify').css('display', 'block');
                $('#btnOTPVerify').css('display', 'none');
                $('#divForgotOTP').css('display', 'block');
                $('#divResendOTP').css('display', 'none');
                $('#xxxMobile').html($("#cpusername").val());


            }
            else {
                swal({
                    title: " " + msg,
                    type: "error",
                });
            }
        },
        error: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        },
        failure: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        }
    });
    HitsCount("Forgot Password");
    return false;
}

function Forgotpasswrod() {
    CheckHotspotAvaiable();
    if ($("#cpusername").val() == "") {
        swal({
            title: "Please enter username !",
            type: "warning",
        });

        return false;
    }

    var url = $('#UrlHomeForgotPasswordOTPGeneration').data('url');
    var data = {};

    data.mobileNo = $("#cpusername").val()
    data.hId = $("#hotspotID").val() != null ? $("#hotspotID").val() : "";
    //data.LocationId = $("#Locid").val() != null ? $("#Locid").val() : "";
    //data.hotspotgroup = $("#hdnhotspotgroup").val() != null ? $("#hdnhotspotgroup").val() : "";
    data.sendexistp = "1";
    data.isUserChar = "1";

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {
            var msg = r.Response;

            if (msg == "SuccessM" || msg == "SuccessB" || msg == "SuccessE" || msg == "Success") {

                $('#btnForgotOTPVerify').css('display', 'block');
                $('#btnOTPVerify').css('display', 'none');
                $('#divForgotOTP').css('display', 'block');
                $('#divResendOTP').css('display', 'none');
                $('#xxxMobile').html($("#cpusername").val());

                swal({
                    title: "Your password has been sent to your registered mobile number.",
                    type: "success",
                });
            }
            else {
                swal({
                    title: " " + msg,
                    type: "error",
                });
            }
        },
        error: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        },
        failure: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        }
    });
    HitsCount("Forgot Password");
    return false;
}

function ForgotPasswordVerifyUpdate() {

    if ($("#cppassword").val() == "") {
        swal({
            title: "Please Enter PIN",
            type: "error",
        });
        return false;
    }

    var url = $('#UrlHomeResendOTPVerification').data('url');
    var data = {};
    data.mobileNo = $("#loginCountry").val() + $("#cpusername").val();
    data.otp = $("#cppassword").val();

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {
            var msg = r.Response;
            if (msg == "Success") {

                $('#btnForgotOTPVerify').css('display', 'block');
                $('#btnOTPVerify').css('display', 'none');

                CheckCustomer();
            }
            else {
                swal({
                    title: "PIN does not match",
                    type: "error",
                });
            }
        }
    });
    return false;
}


function RenewOnline(obj) {
    CheckHotspotAvaiable();
    if ($("#cpusername").val() == "") {
        swal({
            title: "Please enter mobile number !",
            type: "warning",
        });
        return false;
    }

    var PlanId = "0";

    if (obj != null && obj.planid != null) {
        PlanId = obj.planid
    }

    if (PlanId == '' || PlanId == '0') {
        return false;
    }

    var url = $('#UrlHomeRenewOnline').data('url');
    var data = {};

    data.mobileNo =  $("#cpusername").val();
    data.PlanId = PlanId;
    data.rId = $("#hdnResellerID").val() != null ? $("#hdnResellerID").val() : "";
    data.hId = $("#hotspotID").val() != null ? $("#hotspotID").val() : "";
    data.PId = $("#hdnPartnerID").val() != null ? $("#hdnPartnerID").val() : "";
    data.LocationId = $("#Locid").val() != null ? $("#Locid").val() : "";
    data.hotspotgroup = "";
    data.mac = $("#ApMacId").val() != null ? $("#ApMacId").val() : "";
    data.country = $("#loginCountry option:selected").text();


    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {
            var msg = r.Response;
            var reqid = r.reqid;
            if (msg == "Success") {
                $("#custStatus").val("Trial");
                $("#onlinePurchaseReciept").val(reqid);
                $("#isEnterpriseuser").val(0);
                $("#hdnhotspotgroup").val("");
                doLogin();
            }
			else if(msg== "ErrorYoucantRenew")
			{
				Authenticate();
			}
            else if (msg == "InavalidMobile") {
                $('#otpevent').val('onlineregistration');
                $("#paidplanid").val(obj.planid);
                doRegister();
                return false;
            }
            else {
                swal({
                    title: msg.replace("Error", ""),
                    type: "error",
                });
            }
        },
        error: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        },
        failure: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        }
    });
    return false;
}



function RenewVoucher() {
    CheckHotspotAvaiable();
    if ($("#cpusername").val() == "") {
        swal({
            title: "Please enter mobile number !",
            type: "warning",
        });
        return false;
    }

    if ($("#vPin").val() == "") {
        swal({
            title: "Please enter voucher PIN",
            type: "warning",
        });
        return false;
    }

    var url = $('#UrlHomeRenewVoucher').data('url');
    var data = {};
    data.mobileNo = $("#loginCountry").val() + $("#cpusername").val();
    data.vPin = $("#vPin").val();
    data.vPassword = "";
    data.hotspotgroup = "";
    data.hId = $("#hotspotID").val() != null ? $("#hotspotID").val() : "";
    data.LocationId = $("#Locid").val() != null ? $("#Locid").val() : "";

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {

            var msg = r.Response;
            if (msg == "Success") {

                $("#cppassword").val(r.password);
                swal({
                    title: "Enjoy Wifi Access!",
                    type: "success",
                });
                $("#hdnhotspotgroup").val("");
                doLogin();
            }
            else if (msg == "Invalid Mobile Number") {
                $("#hdnhotspotgroup").val("");
                $('#otpevent').val('voucherregistration');
                doRegister();
                return false;
            }
            else {
                swal({
                    title: " " + msg,
                    type: "error",
                });
            }

        },
        error: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        },
        failure: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        }
    });
    return false;
}
function CheckPreRegisteredUser() {

    var url = $('#UrlCheckPreRegisteredUser').data('url');
    var data = {};
    data.mobileNo = $("#loginCountry").val() + $("#cpusername").val();
    data.hotspotgroup = $("#hdnhotspotgroup").val();
    data.hotspot = $("#hotspotID").val() != null ? $("#hotspotID").val() : "";
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function (r) {
            var msg = r.Response;
            if (msg == "Success") {
                FillDetail();
                return false;
            }
            else {
                swal({
                    title: "You are not a Pre registered User, Please contact Administrator.",
                    type: "error",
                });
                return false
            }
        },
        error: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        },
        failure: function (r) {
            var msg = r.Response;
            swal({
                title: " " + msg,
                type: "error",
            });
        }
    });
    return false;
}
function CheckHotspotAvaiable() {

    if ($("#hotspotID").val() == null || $("#hotspotID").val() == "") {
        swal({
            title: "Hotspot not registered.",
            type: "error",
        });
        $('input[type="submit"]').attr("disabled", "disabled");
        $('input[type="submit"]').css("cursor", "none");
        $('input[type="button"]').attr("disabled", "disabled");
        $('input[type="button"]').css("cursor", "none");
        $('input[type="text"]').attr("disabled", "disabled");
        $('input[type="text"]').css("cursor", "none");
        $('input[type="tel"]').attr("disabled", "disabled");
        $('input[type="tel"]').css("cursor", "none");
        $('button[type="button"]').attr("disabled", "disabled");
        $('button[type="button"]').css("cursor", "none");
    }
    return false;

}
