#!/bin/bash

systemctl stop NetworkManager
kill -9 $(pgrep wpa_supplicant)

iw phy0 interface add phish0 type managed addr 12:34:56:78:90:ab
sleep 2
ip ad add 10.66.0.1/24 dev phish0
sleep 2
ip link set phish0 up
sleep 2

cp /etc/dnsmasq.conf{,.bak}
cp ./dnsmasq.conf /etc/dnsmasq.conf

cp hostapd.conf{.tmplt,}

echo ssid=$1 >> hostapd.conf

hostapd -B hostapd.conf

dnsmasq &

./app.py 10.66.0.1 80 &> ./server.log &

