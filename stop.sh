#!/bin/bash

PHISH_ION_HOME=.

kill $(pgrep -f app.py)

kill $(pgrep hostapd)
rm $PHISH_ION_HOME/hostapd.conf

kill $(pgrep dnsmasq)
mv /etc/dnsmasq.conf{.bak,}

iw phish0 interface del
ip link set wlp2s0 down
iw wlp2s0 interface del
iw phy0 interface add wlp2s0 type managed addr 88:b1:11:03:d9:d2
ip link set wlp2s0 up

systemctl start NetworkManager

